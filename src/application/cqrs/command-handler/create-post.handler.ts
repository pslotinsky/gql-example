import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';

import { PostRepository } from 'src/domain/repository/post.repository';
import { CreatePostCommand } from '../command/create-post.command';

@CommandHandler(CreatePostCommand)
export class CreatePostHandler implements ICommandHandler<CreatePostCommand> {
    @Inject('PostRepository')
    private postRepository: PostRepository;

    public async execute({ input }: CreatePostCommand): Promise<any> {
        await this.postRepository.add(input);
    }    
}
