import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';

import { LockRepository } from 'src/domain/repository/lock.repository';
import { UnlockPostCommand } from '../command/unlock-post.command';

@CommandHandler(UnlockPostCommand)
export class UnlockPostHandler implements ICommandHandler<UnlockPostCommand> {
    @Inject('LockRepository')
    private lockRepository: LockRepository;

    public async execute({ postId, userId }: UnlockPostCommand): Promise<any> {
        const lock = await this.lockRepository.getByPostIdOrFail(postId);

        lock.assertUserId(userId);

        await this.lockRepository.remove(lock.id);
    }    
}
