import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';

import { UserRepository } from 'src/domain/repository/user.repository';
import { CreateUserCommand } from '../command/create-user.command';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
    @Inject('UserRepository')
    private userRepository: UserRepository;

    public async execute({ input }: CreateUserCommand): Promise<void> {
        await this.userRepository.add(input);
    }
}
