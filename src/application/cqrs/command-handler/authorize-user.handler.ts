import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';

import { AuthorizeUserCommand } from '../command/authorize-user.command';

export let authorizedUser: string | undefined;

@CommandHandler(AuthorizeUserCommand)
export class AuthorizeUserHandler implements ICommandHandler<AuthorizeUserCommand> {
    public async execute({ id }: AuthorizeUserCommand): Promise<void> {
        authorizedUser = id;
    }
}
