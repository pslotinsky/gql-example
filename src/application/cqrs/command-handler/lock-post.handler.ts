import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { nanoid } from 'nanoid';

import { LockRepository } from 'src/domain/repository/lock.repository';
import { LockPostCommand } from '../command/lock-post.command';

const NANOID_LENGTH = 8;

@CommandHandler(LockPostCommand)
export class LockPostHandler implements ICommandHandler<LockPostCommand> {
    @Inject('LockRepository')
    private lockRepository: LockRepository;

    public async execute({ postId, userId }: LockPostCommand): Promise<any> {
        const lock = await this.lockRepository.getByPostId(postId);

        if (lock) {
            lock.assertUserId(userId);
            await this.lockRepository.remove(lock.id);
        }

        await this.lockRepository.add({ id: nanoid(NANOID_LENGTH), postId, userId });
    }    
}
