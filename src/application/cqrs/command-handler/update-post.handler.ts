import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';

import { Post } from 'src/domain/entity/post.entity';
import { LockRepository } from 'src/domain/repository/lock.repository';
import { PostRepository } from 'src/domain/repository/post.repository';
import { UpdatePostCommand } from '../command/update-post.command';

@CommandHandler(UpdatePostCommand)
export class UpdatePostHandler implements ICommandHandler<UpdatePostCommand> {
    @Inject('LockRepository')
    private lockRepository: LockRepository;

    @Inject('PostRepository')
    private postRepository: PostRepository;

    public async execute(command: UpdatePostCommand): Promise<void> {
        const post = await this.postRepository.getOrFail(command.input.id);

        if (Post.hasVersionedFields(command.input)) {
            await this.versionedUpdate(post, command);
        } else {
            await this.directUpdate(post, command);
        }
    }

    private async versionedUpdate(post: Post, command: UpdatePostCommand): Promise<void> {
        const { input, userId } = command;
        const { id, lockId, version } = input;

        const lock = await this.lockRepository.getByPostIdOrFail(id);

        lock.assertId(lockId);
        lock.assertUserId(userId);
        post.assertVersion(version);

        post.increaseVersion();

        await this.directUpdate(post, command);
    }

    private async directUpdate(post: Post, command: UpdatePostCommand): Promise<void> {
        const { id, ...data } = command.input;

        post.update(data);

        await this.postRepository.update(id, post);
    }
}
