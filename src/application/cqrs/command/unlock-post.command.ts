import { UnlockPostInput } from 'src/application/graphql';

export class UnlockPostCommand implements UnlockPostInput {
    constructor(
        public postId: string,
        public userId: string,
    ) { }
}
