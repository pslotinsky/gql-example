import { UpdatePostInput } from 'src/application/graphql';

export class UpdatePostCommand {
    constructor(
        public input: UpdatePostInput,
        public userId: string,
    ) {}
}
