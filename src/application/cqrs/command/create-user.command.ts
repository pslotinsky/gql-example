import { CreateUserInput } from 'src/application/graphql';

export class CreateUserCommand {
    constructor(
        public input: CreateUserInput,
    ) {}
}
