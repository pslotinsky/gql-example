import { LockPostInput } from 'src/application/graphql';

export class LockPostCommand implements LockPostInput {
    constructor(
        public postId: string,
        public userId: string,
    ) { }
}
