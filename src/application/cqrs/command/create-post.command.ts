import { CreatePostInput } from 'src/application/graphql';

export class CreatePostCommand {
    constructor(
        public input: CreatePostInput,
    ) {}
}
