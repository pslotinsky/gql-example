import { Inject } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';

import { User } from 'src/application/graphql';
import { UserRepository } from 'src/domain/repository/user.repository';
import { FindUsersQuery } from '../query/find-users.query';

@QueryHandler(FindUsersQuery)
export class FindUsersHandler implements IQueryHandler<FindUsersQuery> {
    @Inject('UserRepository')
    private userRepository: UserRepository;

    public async execute(query: FindUsersQuery): Promise<User[]> {
        return this.userRepository.find();
    }
}
