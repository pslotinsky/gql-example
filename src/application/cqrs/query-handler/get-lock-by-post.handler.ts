import { Inject } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Lock } from 'src/domain/entity/lock.entity';

import { LockRepository } from 'src/domain/repository/lock.repository';
import { GetLockByPostQuery } from '../query/get-lock-by-post.query';

@QueryHandler(GetLockByPostQuery)
export class GetLockByPostHandler implements IQueryHandler<GetLockByPostQuery> {
    @Inject('LockRepository')
    private lockRepository: LockRepository;

    public async execute({ postId }: GetLockByPostQuery): Promise<Lock> {
        return this.lockRepository.getByPostIdOrFail(postId);
    }
}
