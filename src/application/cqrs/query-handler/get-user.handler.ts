import { Inject } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';

import { User } from 'src/application/graphql';
import { UserRepository } from 'src/domain/repository/user.repository';
import { GetUserQuery } from '../query/get-user.query';

@QueryHandler(GetUserQuery)
export class GetUserHandler implements IQueryHandler<GetUserQuery> {
    @Inject('UserRepository')
    private userRepository: UserRepository;

    public async execute({ id }: GetUserQuery): Promise<User> {
        return this.userRepository.getOrFail(id);
    }
}
