import { Inject } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';

import { Post } from 'src/domain/entity/post.entity';
import { PostRepository } from 'src/domain/repository/post.repository';
import { GetPostQuery } from '../query/get-post.query';

@QueryHandler(GetPostQuery)
export class GetPostHandler implements IQueryHandler<GetPostQuery> {
    @Inject('PostRepository')
    private postRepository: PostRepository;

    public async execute({ id }: GetPostQuery): Promise<Post> {
        return this.postRepository.getOrFail(id);
    }
}
