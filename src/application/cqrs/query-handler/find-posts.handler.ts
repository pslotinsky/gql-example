import { Inject } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';

import { Post } from 'src/domain/entity/post.entity';
import { PostRepository } from 'src/domain/repository/post.repository';
import { FindPostsQuery } from '../query/find-posts.query';

@QueryHandler(FindPostsQuery)
export class FindPostHandler implements IQueryHandler<FindPostsQuery> {
    @Inject('PostRepository')
    private postRepository: PostRepository;

    public async execute(query: FindPostsQuery): Promise<Post[]> {
        return this.postRepository.find();
    }
}
