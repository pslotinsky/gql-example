import { Inject } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';

export abstract class AbstractResolver {
    @Inject()
    protected commandBus: CommandBus;

    @Inject()
    protected queryBus: QueryBus;
}
