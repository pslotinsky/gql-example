import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { authorizedUser } from '../cqrs/command-handler/authorize-user.handler';

export interface SessionUser {
    id: string;
}

export const AuthUser = createParamDecorator(
    (data: unknown, ctx: ExecutionContext): SessionUser => {
        // const request = ctx.switchToHttp().getRequest();
        // return request.user;
        return { id: authorizedUser };
    },
);
