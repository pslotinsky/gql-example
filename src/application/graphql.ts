
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface AuthorizeUserInput {
    id: string;
}

export interface CreateUserInput {
    id: string;
    firstName: string;
    lastName: string;
}

export interface CreatePostInput {
    id: string;
    userId: string;
}

export interface UpdatePostInput {
    id: string;
    title?: Nullable<string>;
    content?: Nullable<string>;
    userId?: Nullable<string>;
    version: number;
    lockId: string;
}

export interface LockPostInput {
    postId: string;
}

export interface UnlockPostInput {
    postId: string;
}

export interface IQuery {
    user(id: string): User | Promise<User>;
    users(): User[] | Promise<User[]>;
    post(id: string): Post | Promise<Post>;
    posts(): Post[] | Promise<Post[]>;
}

export interface IMutation {
    createUser(createUserInput: CreateUserInput): User | Promise<User>;
    authorizeUser(authorizeUserInput: AuthorizeUserInput): User | Promise<User>;
    createPost(createPostInput: CreatePostInput): Post | Promise<Post>;
    updatePost(updatePostInput: UpdatePostInput): Post | Promise<Post>;
    lockPost(lockPostInput: LockPostInput): Post | Promise<Post>;
    unlockPost(unlockPostInput: UnlockPostInput): Post | Promise<Post>;
}

export interface User {
    id: string;
    firstName: string;
    lastName: string;
}

export interface Post {
    id: string;
    title: string;
    content: string;
    version: number;
    user: User;
    lock?: Nullable<Lock>;
}

export interface Lock {
    id: string;
    user: User;
    post: Post;
}

type Nullable<T> = T | null;
