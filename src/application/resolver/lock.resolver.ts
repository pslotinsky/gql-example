import { Parent, Resolver, ResolveField } from '@nestjs/graphql';

import { Lock } from 'src/domain/entity/lock.entity';
import { AbstractResolver } from '../core/abstract.resolver';
import { GetPostQuery, GetUserQuery } from '../cqrs/query';

@Resolver('Lock')
export class LockResolver extends AbstractResolver {
    @ResolveField('user')
    public resolveUser(
        @Parent() { userId }: Lock,
    ) {
        return this.queryBus.execute(new GetUserQuery(userId));
    }

    @ResolveField('post')
    public resolvePost(
        @Parent() { postId }: Lock,
    ) {
        return this.queryBus.execute(new GetPostQuery(postId));
    }
}
