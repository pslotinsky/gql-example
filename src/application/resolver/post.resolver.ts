import { Args, Mutation, Query, Parent, Resolver, ResolveField } from '@nestjs/graphql';

import { Post } from 'src/domain/entity/post.entity';
import { AbstractResolver } from '../core/abstract.resolver';
import { AuthUser, SessionUser } from '../core/auth-user.decorator';
import { CreatePostCommand, LockPostCommand, UnlockPostCommand, UpdatePostCommand } from '../cqrs/command';
import { FindPostsQuery, GetLockByPostQuery, GetPostQuery, GetUserQuery } from '../cqrs/query';
import { CreatePostInput, LockPostInput, UnlockPostInput, UpdatePostInput } from '../graphql';

@Resolver('Post')
export class PostResolver extends AbstractResolver {
    @Query('posts')
    public async find() {
        return this.queryBus.execute(new FindPostsQuery());
    }

    @Query('post')
    public async getOrFail(
        @Args('id') id: string,
    ) {
        return this.queryBus.execute(new GetPostQuery(id));
    }

    @Mutation('createPost')
    public async create(
        @Args('createPostInput') createPostInput: CreatePostInput,
    ) {
        await this.commandBus.execute(new CreatePostCommand(createPostInput));
        return this.queryBus.execute(new GetPostQuery(createPostInput.id));
    }

    @Mutation('updatePost')
    public async update(
        @Args('updatePostInput') updatePostInput: UpdatePostInput,
        @AuthUser() user: SessionUser,
    ) {
        await this.commandBus.execute(new UpdatePostCommand(updatePostInput, user.id));
        return this.queryBus.execute(new GetPostQuery(updatePostInput.id));
    }

    @Mutation('lockPost')
    public async lock(
        @Args('lockPostInput') { postId }: LockPostInput,
        @AuthUser() user: SessionUser,
    ) {
        await this.commandBus.execute(new LockPostCommand(postId, user.id));
        return this.queryBus.execute(new GetPostQuery(postId));
    }

    @Mutation('unlockPost')
    public async unlock(
        @Args('unlockPostInput') { postId }: UnlockPostInput,
        @AuthUser() user: SessionUser,
    ) {
        await this.commandBus.execute(new UnlockPostCommand(postId, user.id));
        return this.queryBus.execute(new GetPostQuery(postId));
    }

    @ResolveField('user')
    public resolveUser(
        @Parent() { userId }: Post,
    ) {
        return this.queryBus.execute(new GetUserQuery(userId));
    }

    @ResolveField('lock')
    public resolveLock(
        @Parent() { id }: Post,
    ) {
        return this.queryBus.execute(new GetLockByPostQuery(id));
    }
}
