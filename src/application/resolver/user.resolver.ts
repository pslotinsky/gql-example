import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { AbstractResolver } from '../core/abstract.resolver';
import { AuthorizeUserCommand, CreateUserCommand } from '../cqrs/command';
import { FindUsersQuery, GetUserQuery } from '../cqrs/query';
import { AuthorizeUserInput, CreateUserInput, User } from '../graphql';

@Resolver('User')
export class UserResolver extends AbstractResolver {
    @Query('users')
    public async find(): Promise<User[]> {
        return this.queryBus.execute(new FindUsersQuery());
    }

    @Query('user')
    public async getOrFail(
        @Args('id') id: string,
    ): Promise<User> {
        return this.queryBus.execute(new GetUserQuery(id));
    }

    @Mutation('createUser')
    public async create(
        @Args('createUserInput') createUserInput: CreateUserInput,
    ): Promise<User> {
        await this.commandBus.execute(new CreateUserCommand(createUserInput));
        return this.queryBus.execute(new GetUserQuery(createUserInput.id));
    }

    @Mutation('authorizeUser')
    public async authorize(
        @Args('authorizeUserInput') { id }: AuthorizeUserInput,
    ) {
        await this.commandBus.execute(new AuthorizeUserCommand(id));
        return this.queryBus.execute(new GetUserQuery(id));
    }
}
