import { Column, PrimaryColumn } from 'typeorm';

export abstract class AbstractModel<I = string> {
    @PrimaryColumn({ type: 'varchar' })
    public id: I;

    @Column({ default: false })
    public isDeleted: boolean;

    @Column({ type: 'timestamptz' })
    public updateTime: Date;

    @Column({ type: 'timestamptz' })
    public creationTime: Date;
}
