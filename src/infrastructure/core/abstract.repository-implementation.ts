import { NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { DeepPartial, FindManyOptions, getRepository, Repository } from 'typeorm';

import { AbstractModel } from './abstract.model';

export abstract class AbstractRepositoryImplementation<M extends AbstractModel, E, FO = Partial<E>> {
    protected ormRepository: Repository<M>;

    public async find(options?: FO): Promise<E[]> {
        const conditions = options ? { isDeleted: false, ...options } : { isDeleted: false };
        const models = await this.ormRepository.find(conditions as FindManyOptions<M>);

        return this.toEntityList(models);
    }

    public async getOrFail(id: M['id']): Promise<E> {
        const model = await this.getModelOrFail(id);

        return this.toEntity(model);
    }

    public async add(entity: Partial<E>): Promise<E> {
        const time = new Date();

        const model = await this.ormRepository.save({
            ...this.toModel(entity),
            creationTime: time,
            updateTime: time,
        } as DeepPartial<M>);

        return this.toEntity(model);
    }

    public async update(id: M['id'], entity: Partial<E>): Promise<void> {
        const model = this.toModel(entity);

        await this.updateModel(id, model);
    }

    public async remove(id: M['id']): Promise<void> {
        await this.updateModel(id, { isDeleted: true } as Partial<M>);
    }

    protected abstract get modelConstructor(): new() => M;

    protected get entityName(): string {
        return this.modelConstructor.name.replace(/Model$/, '');
    }

    protected async updateModel(id: M['id'], model: Partial<M>): Promise<void> {
        await this.getModelOrFail(id);

        const updateTime = new Date();

        await this.ormRepository.update(id, { ...model, updateTime } as any);
    }

    protected async getModelOrFail(id: M['id']): Promise<M> {
        const model = await this.ormRepository.findOne(id);

        if (!model || model.isDeleted) {
            throw new NotFoundException(`${this.entityName} ${id} not found`);
        }

        return model;
    }

    protected toEntityList(models: M[]): E[] {
        return models.map(model => this.toEntity(model));
    }

    protected createOrmRepository(): Repository<M> {
        return getRepository(this.modelConstructor);
    }

    protected toModel(entity: Partial<E>): Partial<M> {
        return plainToClass(this.modelConstructor, entity);
    }

    protected abstract toEntity(model: M): E;
}
