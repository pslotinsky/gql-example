import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Lock } from 'src/domain/entity/lock.entity';
import { LockRepository } from 'src/domain/repository/lock.repository';
import { AbstractRepositoryImplementation } from '../core/abstract.repository-implementation';
import { LockModel } from '../model/lock.model';

@Injectable()
export class LockRepositoryImplementation
    extends AbstractRepositoryImplementation<LockModel, Lock>
    implements LockRepository {

    @InjectRepository(LockModel)
    protected ormRepository: Repository<LockModel>;

    public async getByPostId(postId: string): Promise<Lock | undefined> {
        const [lock] = await this.find({ postId });

        return lock;
    }

    public async getByPostIdOrFail(postId: string): Promise<Lock> {
        const lock = await this.getByPostId(postId);

        if (!lock) {
            throw new NotFoundException(`${this.entityName} not found for post ${postId}`);
        }

        return lock;
    }

    protected get modelConstructor(): new () => LockModel {
        return LockModel;
    }

    protected toEntity(model: LockModel): Lock {
        return plainToClass(Lock, model);
    }
}
