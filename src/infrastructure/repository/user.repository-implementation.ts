import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';

import { User } from 'src/domain/entity/user.entity';
import { UserRepository } from 'src/domain/repository/user.repository';
import { AbstractRepositoryImplementation } from '../core/abstract.repository-implementation';
import { UserModel } from '../model/user.model';

@Injectable()
export class UserRepositoryImplementation
    extends AbstractRepositoryImplementation<UserModel, User>
    implements UserRepository {

    @InjectRepository(UserModel)
    protected ormRepository: Repository<UserModel>;

    protected get modelConstructor(): new () => UserModel {
        return UserModel;
    }

    protected toEntity(model: UserModel): UserModel {
        return plainToClass(UserModel, model);
    }
}
