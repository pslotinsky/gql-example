import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';

import { Post } from 'src/domain/entity/post.entity';
import { PostRepository } from 'src/domain/repository/post.repository';
import { AbstractRepositoryImplementation } from '../core/abstract.repository-implementation';
import { PostModel } from '../model/post.model';

@Injectable()
export class PostRepositoryImplementation
    extends AbstractRepositoryImplementation<PostModel, Post>
    implements PostRepository {

    @InjectRepository(PostModel)
    protected ormRepository: Repository<PostModel>;

    protected get modelConstructor(): new () => PostModel {
        return PostModel;
    }

    protected toEntity(model: PostModel): Post {
        return plainToClass(Post, model);
    }
}
