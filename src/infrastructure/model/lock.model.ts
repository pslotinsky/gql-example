import { Column, Entity } from 'typeorm';

import { AbstractModel } from '../core/abstract.model';

@Entity('lock')
export class LockModel extends AbstractModel {
    @Column()
    public postId: string;

    @Column()
    public userId: string;
}
