import { Column, Entity } from 'typeorm';

import { AbstractModel } from '../core/abstract.model';

@Entity('post')
export class PostModel extends AbstractModel {
    @Column({ default: '' })
    public title: string;

    @Column({ default: '' })
    public content: string;

    @Column({ default: 1 })
    public version: number;

    @Column()
    public userId: string;
}
