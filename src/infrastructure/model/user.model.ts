import { Column, Entity } from 'typeorm';

import { AbstractModel } from '../core/abstract.model';

@Entity('user')
export class UserModel extends AbstractModel {
    @Column()
    public firstName: string;

    @Column()
    public lastName: string;
}
