import { Module, Provider } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { join } from 'path';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import { LockResolver } from './application/resolver/lock.resolver';
import { PostResolver } from './application/resolver/post.resolver';
import { UserResolver } from './application/resolver/user.resolver';
import { LockModel } from './infrastructure/model/lock.model';
import { PostModel } from './infrastructure/model/post.model';
import { UserModel } from './infrastructure/model/user.model';
import { LockRepositoryImplementation } from './infrastructure/repository/lock.repository-implementation';
import { PostRepositoryImplementation } from './infrastructure/repository/post.repository-implementation';
import { UserRepositoryImplementation } from './infrastructure/repository/user.repository-implementation';
import {
  AuthorizeUserHandler,
  CreatePostHandler,
  CreateUserHandler,
  LockPostHandler,
  UnlockPostHandler,
  UpdatePostHandler,
} from './application/cqrs/command-handler';
import {
  FindPostHandler,
  FindUsersHandler,
  GetLockByPostHandler,
  GetPostHandler,
  GetUserHandler,
} from './application/cqrs/query-handler';

const {
  DB_HOST = 'localhost',
  DB_PORT = '5432',
  DB_USERNAME = 'gorod',
  DB_PASSWORD = '123qwe',
  DB_NAME = 'gql_example',
} = process.env;

const entities = [
  UserModel,
  PostModel,
  LockModel,
];

const commandHandlers = [
  AuthorizeUserHandler,
  CreateUserHandler,
  CreatePostHandler,
  UpdatePostHandler,
  LockPostHandler,
  UnlockPostHandler,
  UpdatePostHandler,
];

const queryHandlers = [
  GetPostHandler,
  GetLockByPostHandler,
  GetUserHandler,
  FindPostHandler,
  FindUsersHandler,
];

const imports = [
  TypeOrmModule.forRoot({
    type: 'postgres',
    host: DB_HOST,
    port: Number(DB_PORT),
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    synchronize: true,
    namingStrategy: new SnakeNamingStrategy(),
    entities,
  }),
  TypeOrmModule.forFeature(entities),
  GraphQLModule.forRoot({
    typePaths: ['./**/*.graphql'],
    definitions: {
      path: join(process.cwd(), 'src/application/graphql.ts'),
    },
  }),
  CqrsModule,
];

const resolvers = [
  UserResolver,
  PostResolver,
  LockResolver,
];

const repositories: Provider[] = [
  { provide: 'UserRepository', useClass: UserRepositoryImplementation },
  { provide: 'LockRepository', useClass: LockRepositoryImplementation },
  { provide: 'PostRepository', useClass: PostRepositoryImplementation },
];

@Module({
  imports,
  providers: [
    ...resolvers,
    ...repositories,
    ...commandHandlers,
    ...queryHandlers,
  ],
})
export class AppModule { }
