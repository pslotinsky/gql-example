import { AbstractEntity } from './abstract.entity';

export interface AbstractRepository<E extends AbstractEntity, FO = Partial<E>> {
    find(options?: FO): Promise<E[]>;
    getOrFail(id: E['id']): Promise<E>;
    add(entity: Partial<E>): Promise<E>;
    update(id: E['id'], entity: Partial<E>): Promise<void>;
    remove(id: E['id']): Promise<void>;
}
