import { AbstractRepository } from '../core/abstract.repository';
import { User } from '../entity/user.entity';

export interface UserRepository extends AbstractRepository<User> {
}
