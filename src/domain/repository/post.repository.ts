import { AbstractRepository } from '../core/abstract.repository';
import { Post } from '../entity/post.entity';

export interface PostRepository extends AbstractRepository<Post> {
}
