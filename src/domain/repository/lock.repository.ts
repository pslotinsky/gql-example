import { AbstractRepository } from '../core/abstract.repository';
import { Lock } from '../entity/lock.entity';
import { Post } from '../entity/post.entity';

export interface LockRepository extends AbstractRepository<Lock> {
    getByPostId(postId: Post['id']): Promise<Lock | undefined>;
    getByPostIdOrFail(postId: Post['id']): Promise<Lock>;
}
