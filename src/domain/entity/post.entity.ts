import { strict as assert } from 'assert';
import { omit } from 'lodash';

import { AbstractEntity } from '../core/abstract.entity';

const UNSAFE_FIELDS = ['id', 'version'];
const VERSIONED_FIELDS = ['title', 'content'];

export class Post extends AbstractEntity {
    public static isVersionedField(field: keyof Post): boolean {
        return VERSIONED_FIELDS.includes(field);
    }

    public static hasVersionedFields(data: Partial<Post>): boolean {
        return Object.keys(data).some(Post.isVersionedField);
    }

    public title: string;
    public content: string;
    public version: number;
    public userId: string;

    public assertVersion(version: number) {
        assert.equal(this.version, version, `Unexpected version ${version} for post ${this.id}`);
    }

    public increaseVersion(): void {
        this.version += 1;
    }

    public update(data: Partial<Post>): void {
        const safeData = omit(data, UNSAFE_FIELDS);
        Object.assign(this, safeData);
    }
}
