import { strict as assert } from 'assert';

import { AbstractEntity } from '../core/abstract.entity';

export class Lock extends AbstractEntity {
    public postId: string;
    public userId: string;

    public assertId(lockId: string): void {
        assert.equal(this.id, lockId, `Unexpected lock ${lockId}`);
    }

    public assertUserId(userId: string): void {
        assert.equal(this.userId, userId, `Lock ${this.id} don't belong to user ${userId}`);
    }
}
