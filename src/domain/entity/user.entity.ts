import { AbstractEntity } from '../core/abstract.entity';

export class User extends AbstractEntity {
    public firstName: string;
    public lastName: string;
}
